#!/usr/bin/env python3

import uuid
import json
import socket
import pprint
import time


def read_msg(s):
    buf = ''
    while True:
        # print("buf: [{}]".format(buf))
        buf += s.recv(256).decode('utf-8')
        idx = 0
        while buf[idx] != '\n' and idx <= len(buf):
            idx += 1
        length = int(buf[0:idx])
        if len(buf[idx + 1:]) >= length:
            # print('***Looks like we have enough data!')
            # print('***buf at this point: [{}]'.format(buf))
            buf = buf[idx + 1:]
            # print('***buf after length is removed: [{}]'.format(buf))
            msg = buf[:length]
            # print('***extracted message: [{}]'.format(msg))
            buf = buf[length:]

            return msg


client_id = str(uuid.uuid4())
payload = json.dumps({
    'client_id': client_id,
    'cmd': 'start_stream',
    'hashtags': ['python']
})

client_msg = '{s}\n{m}'.format(s=len(payload), m=payload).encode('utf-8')

print('Configuring initial socket connection')
init_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
init_socket.connect(('0.0.0.0', 5000))
print('Sending stream connection request')
init_socket.send(client_msg)

d = read_msg(init_socket)
response = json.loads(d)
print('Got response from server:')
pprint.pprint(response)

print('Closing initial socket')
init_socket.close()

new_port = int(response.get('new_port'))
print('Opening new socket on port ' + str(new_port))
time.sleep(5)
stream_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
stream_socket.connect(('0.0.0.0', new_port))
print('Made new connection on port ' + str(new_port))

while True:
    server_msg = read_msg(stream_socket)
    data = json.loads(server_msg)
    print("Got tweet from server: " + data.get("text"))
