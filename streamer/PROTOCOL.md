# Client-server communiation protocol
The communication protocol is fairly straightforward. A packet is formatted as follows:

```
<length of message>
<JSON message>
```

## Notes
* The message length is calculated on the *JSON string*, so any length calculations should be done after preparing the string.
