#!/usr/bin/env python3

from multiprocessing import Process
import json
import socket
import logging
import pprint
import tweepy

from streamer import Streamer


class Server(object):
    logging.basicConfig()

    def __init__(self, host='0.0.0.0', well_known_port=5050, config={}):
        # Configure server with parameters
        self.host = host
        self.well_known_port = well_known_port
        self.config = config

        self.auth = tweepy.OAuthHandler(config.get("consumer_key"),
                                        config.get("consumer_secret"))
        self.auth.set_access_token(config.get("access_token"),
                                   config.get("access_secret"))

        # Set up processes dict
        self.processes = {}

        # Configure logging
        self.logger = logging.getLogger('Server')
        self.logger.setLevel(logging.DEBUG)
        self.logger.info('Server ready to run')

    def run(self):
        """
        Run the main loop of the server, listening for connections and
        assigning port numbers for communication.
        """
        # Listen on well-known port for connections
        self.logger.info('Listening at {h}:{p}'.format(h=self.host,
                                                       p=self.well_known_port))
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 5)
        sock.bind((self.host, self.well_known_port))
        sock.listen()

        while True:
            # Accept client connections
            (client, addr) = sock.accept()
            self.logger.info('New connection on {addr}'.format(addr=addr))

            """
            TODO: refactor to use packet-size-agnostic loading, similarly to
            how the test_client.py currently does it.
            """
            msg = self.read_msg(client, addr)
            if msg is not None:
                self.logger.debug('Got message from client: {m}'.format(m=msg))

                # Get client ID
                client_data = json.loads(msg)
                self.logger.info('Got new client request')
                self.logger.info(pprint.pformat(client_data))

                client_id = client_data.get('client_id')
                client_cmd = client_data.get('cmd')

                if client_cmd == 'start_stream':
                    self.add_stream(client_id, client, client_data)
                    self.logger.debug('Stream started for client {i}'.format(i=client_id))
                elif client_cmd == 'stop_stream':
                    self.stop_stream(client_id, client)
                    self.logger.debug('Stream stopped for client {i}'.format(i=client_id))
            else:
                self.logger.debug('Client at {a} disappeared without sending'.format(a=addr))

            client.close()

    @staticmethod
    def get_open_port():
        """
        Get a currently open and available port number.
        """
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('', 0))
        addr, port = s.getsockname()
        s.close()
        return port

    def stream(self, port, client_info):
        """
        Stream data from Twitter.

        :param port:
        :param client_info:
        """
        self.logger.info('Listening on {port}'.format(port=port))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((self.host, port))
        s.listen()
        (client, addr) = s.accept()
        self.logger.info('Got client connection on {addr}'.format(addr=addr))

        hashtags = client_info.get("hashtags")

        self.logger.info('Streaming for hashtags {h}'.format(h=hashtags))

        stream = tweepy.Stream(self.auth,
                               Streamer(client,
                                        addr,
                                        client_info.get('client_id')))
        try:
            stream.filter(track=hashtags)
        except BrokenPipeError:
            self.logger.info('Broken pipe for client {i}'.format(i=client_info.get('client_id')))
        except ConnectionResetError:
            self.logger.info('Connection reset by {i}'.format(i=client_info.get('client_id')))

    def add_stream(self, client_id, client, client_data):
        """
        Add a new stream.

        :param client_id:
        :param client:
        :param client_data:
        """
        # Switch to new port
        new_port = self.get_open_port()
        self.logger.info('client {i} -> port {p}'.format(p=new_port,
                                                         i=client_id))

        self.processes[client_id] = {
            "process": Process(target=self.stream,
                               args=(new_port,
                                     client_data)),
            "port": new_port
        }

        self.processes.get(client_id).get("process").start()

        self.logger.debug('Sending client new port')
        client.send(self.prepare_message({
            'new_port': new_port
        }))

    def stop_stream(self, client_id, client):
        """
        Stop a currently running stream.

        :param client_id:
        :param client:
        """
        self.logger.info('Stopping stream for client {i}'.format(i=client_id))

        if self.processes.get(client_id) is not None:
            self.processes.get(client_id).get('process').terminate()
            client.send(self.prepare_message({
                'result': 'stream_terminated'
            }))
        else:
            client.send(self.prepare_message({
                'result': 'stream_not_found'
            }))

    def read_msg(self, client, addr):
        """
        Read a message from a client.

        :param client:
        :param addr:
        """
        self.logger.debug('Receiving from client at {a}'.format(a=addr))
        buf = ''
        while True:
            buf += client.recv(256).decode('utf-8')
            idx = 0

            try:
                while idx <= len(buf) and buf[idx] != '\n':
                    idx += 1
            except IndexError:
                return None

            length = int(buf[0:idx])

            if len(buf[idx:]) >= length:
                buf = buf[idx + 1:]
                msg = buf[:length]
                buf = buf[length:]

                return msg

    def prepare_message(self, payload):
        """
        Prepare a message for sending to the client.

        :param payload:
        """
        msg = json.dumps(payload)
        return '{s}\n{m}'.format(s=len(msg), m=msg).encode('utf-8')
