#!/usr/bin/env python3

from argparse import ArgumentParser
import yaml
from server import Server


def parse_args():
    argp = ArgumentParser(prog='migratory-avians')
    argp.add_argument('--host',
                      help='',
                      default='0.0.0.0')
    argp.add_argument('--port',
                      help='well-known port to listen on',
                      type=int,
                      default=5000)
    argp.add_argument('--config',
                      help='path to config file',
                      required=True)
    return argp.parse_args()


if __name__ == '__main__':
    args = parse_args()
    config = yaml.safe_load(open(args.config, 'r'))
    s = Server(host=args.host,
               well_known_port=int(args.port),
               config=config)
    s.run()
