import logging
import json
import tweepy


class Streamer(tweepy.StreamListener):
    def __init__(self, client, addr, client_id):
        super().__init__()

        self.client_id = client_id
        self.client = client
        self.addr = addr

        self.logger = logging.getLogger('TweetStreamer_{cid}'.format(cid=self.client_id))
        self.logger.setLevel(logging.DEBUG)

    def on_status(self, status):
        # self.logger.debug('Got status')

        msg = json.dumps(status._json)
        payload = '{length}\n{m}'.format(length=len(msg), m=msg)
        self.client.send(payload.encode('utf-8'))

        return True

    def on_error(self, status_code):
        self.logger.error('Error: status_code={}'.format(status_code))
        return True

    def on_timeout(self):
        self.logger.error('Error: timeout')
        return True
