# Migratory Avians
## Overview
This project is a [Twitter](https://twitter.com/) streaming project. It has two parts: the server and the client.

### Server
The server is a [Python](https://www.python.org/) program that leverages the [tweepy](http://www.tweepy.org/) library for streaming data from Twitter. The current version supports streaming by hashtags.

### Client
The client is a [C++](https://isocpp.org/)/[Qt](https://www.qt.io/) graphical desktop application that communicates with the server to stream data from Twitter, and to plot it (unimplemented) on a 2D (eventually, also 3D) projection of the earth, based on the geolocation data from the Twitter API.

The client also logs events, such as server connections and tweets being received, to the "Events" tab in the GUI.

## Dependencies
### Server
The server requires Python 3+, with the following libraries installed (also found in `streamer/requirements.txt`):
* certifi==2018.4.16
* chardet==3.0.4
* idna==2.7
* oauthlib==2.1.0
* PySocks==1.6.8
* PyYAML==3.13
* requests==2.19.1
* requests-oauthlib==1.0.0
* six==1.11.0
* tweepy==3.6.0
* urllib3==1.23

### Client
The client requires Qt 5.11, and the [yaml-cpp](https://github.com/jbeder/yaml-cpp) library. To install the `yaml-cpp` library on Manjaro/Arch Linux, run `sudo pacman -S yaml-cpp`.
