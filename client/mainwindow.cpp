#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "serverconnectiondialog.h"
#include "serverinfo.h"

#include "yaml-cpp/yaml.h"

#include <QDebug>
#include <QDialog>
#include <QFileInfo>
#include <QFile>
#include <QDir>
#include <QFileDialog>
#include <QJsonObject>
#include <QListWidgetItem>
#include <QTcpSocket>
#include <QJsonArray>
#include <QUuid>
#include <QJsonDocument>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  this->ui->tabWidget->setCurrentIndex(0);

  // Start off with no connections
  this->currentServer = NULL;

  // Set application directory
  this->appDir = QDir::homePath() + QDir::separator() + ".migav";

  // Server socket
  this->serverSocket = new QTcpSocket(this);
  connect(this->serverSocket, &QIODevice::readyRead, this,
	  &MainWindow::handleServerData);
  connect(this->serverSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
	  this, &MainWindow::handleServerError);
  
  // Client ID
  this->clientID = QUuid::createUuid();

  this->streaming = false;
}

MainWindow::~MainWindow()
{
  delete ui;

  // Make sure we delete the currentServer connection, no memory leaks!
  delete this->currentServer;
}

/*
 * Quit the application.
 */
void MainWindow::on_actionQuit_triggered()
{
  //this->disconnectFromServer();
  this->close();
}

/*
 * Handle the user's new connection action.
 */
void MainWindow::on_actionNew_Connection_triggered()
{
  this->serverConnectionDialog = new ServerConnectionDialog();
  if (this->serverConnectionDialog->exec() == QDialog::Accepted)
  {
    qDebug() << "Changes saved";

    if (this->currentServer != NULL)
    {
      qDebug() << "Clearing previous server info";
      delete this->currentServer;
    }

    this->currentServer = new ServerInfo;
    this->currentServer->name = this->serverConnectionDialog->getName();
    this->currentServer->address = this->serverConnectionDialog->getAddress();
    this->currentServer->port = this->serverConnectionDialog->getPort();

    if (this->saveCurrentServer())
    {
      qDebug() << "Saved to file";
      this->displayServerConnection();
    }
    else
    {
      qDebug() << "Could not save to file";
    }
  }
  else
  {
    qDebug() << "Changes discarded";
  }
}

/*
 * Handle the open connection action. This is intended to hanle the user
 * wanting to load a server connection file to a know and previously used
 * server, as opposed to generating a new connection.
 */
void MainWindow::on_actionOpen_Connection_triggered()
{
  QString path = QFileDialog::getOpenFileName(this, "Open Server File",
					      this->appDir,
					      "YAML (*.yml *.yaml)");

  
  qDebug() << "Opening file at" << path;

  QFile f(path);
  if (f.open(QIODevice::ReadOnly))
  {
    QTextStream in(&f);

    QString contents = in.readAll();
    YAML::Node node = YAML::Load(contents.toStdString());

    if (! node["name"] ||
	! node["address"] ||
	! node["port"])
    {
      qDebug() << "Failed to load file, invalid format";
    }

    QString name = QString::fromStdString(node["name"].as<std::string>());
    QString address = QString::fromStdString(node["address"].as<std::string>());
    unsigned port = node["port"].as<unsigned>();

    if (this->currentServer != NULL)
    {
      delete this->currentServer;
    }

    this->currentServer = new ServerInfo;
    this->currentServer->name = name;
    this->currentServer->address = address;
    this->currentServer->port = port;

    this->displayServerConnection();
  }
  else
  {
    qDebug() << "Could not read from file, error was" << f.errorString();
  }
}

/*
 * Handle the editing of the current server connection.
 */
void MainWindow::on_actionEdit_Current_Connection_triggered()
{
  if (this->currentServer != NULL)
  {
    
  }
}

/*
 * Handle entry of new hashtags into the list.
 */
void MainWindow::on_hashtagEntryField_returnPressed()
{
  QListWidgetItem* newHashtag = new QListWidgetItem;
  newHashtag->setText(this->ui->hashtagEntryField->text());
  this->ui->hashtagList->insertItem(0, newHashtag);
  this->ui->hashtagEntryField->clear();

  if (this->streaming == true)
  {
    //this->disconnectFromServer();
    this->streaming = false;
  }
  
  this->connectToServer();
}

/*
 * Handle removal of hashtags from the list.
*/
void MainWindow::on_hashtagList_itemDoubleClicked(QListWidgetItem* item)
{
  int row = this->ui->hashtagList->row(item);
  this->ui->hashtagList->takeItem(row);
  delete item;

  this->connectToServer();
}

void MainWindow::on_reconnectButton_clicked()
{
  this->connectToServer();
}


/*
 * Save the currently loaded server to a file. This assumes the currentServer
 * attribute is not set to null.
 * 
 * @return boolean truth of the operation's success
 */
bool MainWindow::saveCurrentServer()
{
  YAML::Emitter yml;
  yml << YAML::BeginMap;
  yml << YAML::Key << "name";
  yml << YAML::Value << this->currentServer->name.toStdString();
  yml << YAML::Key << "address";
  yml << YAML::Value << this->currentServer->address.toStdString();
  yml << YAML::Key << "port";
  yml << YAML::Value << this->currentServer->port;
  yml << YAML::EndMap;

  QString filename = this->currentServer->name.replace(" ", "_") + ".yml";
  QString path = appDir + QDir::separator() + filename;

  this->ui->statusText->setText("Checking for application directory " + appDir);
  if (! QFileInfo::exists(this->appDir))
  {
    this->ui->statusText->setText("Creating application directory");
    if (! QDir().mkdir(appDir))
    {
      this->ui->statusText->setText("Could not create application directory");
      return false;
    }
  }

  QFile f(path);
  if (f.open(QIODevice::WriteOnly))
  {
    QTextStream s(&f);
    s << yml.c_str();
  }
  else
  {
    this->ui->statusText->setText("Could not open file");
    return false;
  }

  return true;
}

void MainWindow::connectToServer()
{
  this->serverSocket->abort();
  this->serverSocket->disconnectFromHost();
  this->serverSocket->connectToHost(this->currentServer->address,
				    this->currentServer->port);

  this->ui->eventsField->append("Connected to server at "
				+ this->currentServer->address + ":"
				+ QString::number(this->currentServer->port));

  QJsonArray hashtags;
  for (int i = 0; i < this->ui->hashtagList->count(); i++)
  {
    hashtags.insert(0, this->ui->hashtagList->item(i)->text());
  }

  qDebug() << "clientID:" << this->clientID.toString();
  QJsonObject obj;
  obj.insert("client_id", this->clientID.toString());
  obj.insert("cmd", "start_stream");
  obj.insert("hashtags", hashtags);
  QJsonDocument doc(obj);

  QString payload = doc.toJson(QJsonDocument::Compact);
  QString msg = QString::number(payload.size()) + "\n" + payload;

  this->ui->statusText->setText("Sending new client request to server");
  this->ui->eventsField->append("Sending new client request to server at "
				+ this->currentServer->address + ":"
				+ QString::number(this->currentServer->port));
  this->serverSocket->write(msg.toUtf8());
  this->streaming = true;
}

void MainWindow::handleServerData()
{
  this->buffer.append(this->serverSocket->readAll());

  int idx = 0;
  while (idx < this->buffer.size() && this->buffer[idx] != '\n')
  {
    idx++;
  }

  if (idx == 0)
  {
    return;
  }

  int length = this->buffer.mid(0, idx).toInt();

  if (this->buffer.mid(idx + 1, -1).size() < length)
  {
    return;
  }

  QJsonDocument doc = QJsonDocument::fromJson(this->buffer.mid(idx + 1, length));
  QJsonObject obj = doc.object();

  if (obj.contains("new_port"))
  {
    unsigned newPort = obj.value("new_port").toInt();

    this->ui->statusText->setText("Got new port from server: "
				  + QString::number(newPort));
    this->ui->eventsField->append("Got new port from server: "
				  + QString::number(newPort));

    this->serverSocket->disconnectFromHost();
    this->serverSocket->connectToHost(this->currentServer->address,
				      newPort);

    this->ui->statusText->setText("Streaming");
    this->ui->eventsField->append("Streaming from server on "
				  + QString::number(newPort));
  }
  else
  {
    if (obj.contains("text"))
    {
      this->ui->eventsField->append("Got Tweet: "
				    + obj.value("text").toString()
				    + "\n");
    }
    for (QJsonObject::iterator iter = obj.begin();
	 iter != obj.end();
	 iter++)
    {
      //qDebug() << iter.key();
    }
  }

  this->buffer = this->buffer.mid(idx + 1, length);
}

void MainWindow::handleServerError(QAbstractSocket::SocketError err)
{
  switch (err)
  {
  case QAbstractSocket::RemoteHostClosedError:
    this->ui->statusText->setText("Server went away");
    this->ui->eventsField->append(this->currentServer->address
				  + ":"
				  + QString::number(this->currentServer->port)
				  + " went away");
    qDebug() << "Server went away";
    break;
  case QAbstractSocket::HostNotFoundError:
    this->ui->statusText->setText("Host not found");
    this->ui->eventsField->append("Host "
				  + this->currentServer->address
				  + ":"
				  + QString::number(this->currentServer->port)
				  + " not found");
    qDebug() << "Host not found";
    break;
  case QAbstractSocket::ConnectionRefusedError:
    this->ui->statusText->setText("Connection refused");
    this->ui->eventsField->append("Host "
				  + this->currentServer->address
				  + ":"
				  + QString::number(this->currentServer->port)
				  + " refused connection");
    qDebug() << "Connection was refused";
    break;
  default:
    this->ui->statusText->setText("Unknown error, see events log");
    this->ui->eventsField->append("Unknown error from server, error was "
				  + this->serverSocket->errorString());
    qDebug() << "Error:" << this->serverSocket->errorString();
    break;
  }

  this->streaming = false;
}

void MainWindow::displayServerConnection()
{
  QString s = this->currentServer->address + ":"
    + QString::number(this->currentServer->port);
  this->ui->serverAddressField->setText(s);
}

void MainWindow::disconnectFromServer()
{
  if (this->streaming == false)
  {
    return;
  }

  QTcpSocket s;
  s.connectToHost(this->currentServer->address,
		  this->currentServer->port);
  
  QJsonObject obj;
  obj.insert("client_id", this->clientID.toString());
  obj.insert("cmd", "stop_stream");
  QJsonDocument doc(obj);

  QString payload = doc.toJson(QJsonDocument::Compact);
  QString msg = QString::number(payload.size()) + "\n" + payload;

  this->ui->statusText->setText("Sending stop request to server");
  this->ui->eventsField->append("Sending stop request to server at "
				+ this->currentServer->address + ":"
				+ QString::number(this->currentServer->port)
				+ "\n");
  qDebug() << "Sending stop request";
  s.write(msg.toUtf8());

  this->streaming = false;
}
