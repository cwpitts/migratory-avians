#ifndef SERVERINFO_H
#define SERVERINFO_H
#include <QString>

struct ServerInfo
{
  /*
   * The address of the server, generally in IP address format.
   */
  QString address;

  /*
   * The name of the server, arbitrarily chosen and used for display only.
   */
  QString name;

  /*
   * The port number of the server.
   */
  unsigned port;
};
#endif //SERVERINFO_H
