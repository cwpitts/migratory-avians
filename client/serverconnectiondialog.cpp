#include "serverconnectiondialog.h"
#include "ui_serverconnectiondialog.h"

#include <QDebug>
#include <QString>

ServerConnectionDialog::ServerConnectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServerConnectionDialog)
{
    ui->setupUi(this);
}

ServerConnectionDialog::~ServerConnectionDialog()
{
  delete ui;
}

QString ServerConnectionDialog::getAddress()
{
  return this->address;
}

QString ServerConnectionDialog::getName()
{
  return this->name;
}

unsigned ServerConnectionDialog::getPort()
{
  return this->port;
}

void ServerConnectionDialog::on_buttonBox_accepted()
{
  this->name = this->ui->nameField->text();
  this->address = this->ui->addressField->text();
  this->port = this->ui->portField->text().toUInt();
  this->accept();
}
