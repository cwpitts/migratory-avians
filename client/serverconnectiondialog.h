#ifndef SERVERCONNECTIONDIALOG_H
#define SERVERCONNECTIONDIALOG_H

#include <QString>
#include <QDialog>

namespace Ui
{
  class ServerConnectionDialog;
}

class ServerConnectionDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ServerConnectionDialog(QWidget *parent = 0);
  ~ServerConnectionDialog();

  QString getAddress();
  QString getName();
  unsigned getPort();

private slots:
  void on_buttonBox_accepted();

private:
  Ui::ServerConnectionDialog *ui;
  QString
  address;
  QString name;
  unsigned port;
};

#endif // SERVERCONNECTIONDIALOG_H
