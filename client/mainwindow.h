#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "serverconnectiondialog.h"
#include "serverinfo.h"

#include <QMainWindow>
#include <QListWidgetItem>
#include <QTcpSocket>
#include <QUuid>
#include <QByteArray>

namespace Ui
{
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = 0);
  ~MainWindow();

private slots:
  void on_actionQuit_triggered();
  void on_actionNew_Connection_triggered();
  void on_actionOpen_Connection_triggered();
  void on_actionEdit_Current_Connection_triggered();
  void on_hashtagEntryField_returnPressed();
  void on_hashtagList_itemDoubleClicked(QListWidgetItem* item);
  void handleServerData();
  void handleServerError(QAbstractSocket::SocketError err);
  void on_reconnectButton_clicked();

private:
  Ui::MainWindow* ui;
  ServerConnectionDialog* serverConnectionDialog;
  struct ServerInfo* currentServer;
  QString appDir;
  QTcpSocket* serverSocket;
  QTcpSocket* streamSocket;
  QUuid clientID;
  QByteArray buffer;
  bool streaming;
  
  bool saveCurrentServer();
  void connectToServer();
  void displayServerConnection();
  void disconnectFromServer();
};

#endif // MAINWINDOW_H
